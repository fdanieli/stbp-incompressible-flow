function sigma = computeSingVals(r,Pb)
% Pb = 4;
Prec = 1;
STsolve = 0;
oU = 2;
oP = 1;
Pe = 1000;

% r  = 6;
mu = 1;

rng(1)

% -> CC'= B*Finv'*B' * ( Minv*Fp*Ainv )'*( Minv*Fp*Ainv ) * B*Finv*B'
function y = precOp( x, Mpinv, Apinv, B, dt, Fuinv, Mu, W, dirIdx )
  Np = size(B,1);
  Nu = size(B,2);
  NT = length(W);
  
  % apply B'
  y0 = zeros(Nu*NT,1);
  for t=1:NT
    y0((1:Nu)+(t-1)*Nu) = B'*x((1:Np)+(t-1)*Np);
  end
  % solve for Fu via time stepping
  yprev = zeros(Nu,1);
  for t=1:NT
    if length(Fuinv) == 1
      y0((1:Nu)+(t-1)*Nu) = Fuinv{1}\( y0((1:Nu)+(t-1)*Nu) + Mu*(yprev/dt) );
    else
      y0((1:Nu)+(t-1)*Nu) = Fuinv{t}\( y0((1:Nu)+(t-1)*Nu) + Mu*(yprev/dt) );
    end
    yprev = y0((1:Nu)+(t-1)*Nu);
  end
  % apply B
  y1 = zeros(Np*NT,1);
  for t=1:NT
    y1((1:Np)+(t-1)*Np) = B*y0((1:Nu)+(t-1)*Nu);
  end
  
  % Minv*Fp*Ainv is block bi-diag, with blocks Apinv/dt+mu*Mpinv and -Apinv/dt
  yAprev = zeros(Np,1);
  for t=1:NT
    yA = Apinv\y1((1:Np)+(t-1)*Np);
    yM = Mpinv\( mu*y1((1:Np)+(t-1)*Np) + W{t}*yA );
%     yM = mu*Mpinv\y1((1:Np)+(t-1)*Np);
    y1((1:Np)+(t-1)*Np) = yA/dt+yM - yAprev/dt;  % yAprev is zero at first it
    yAprev = yA;
    yAprev(dirIdx) = 0; % kill contributions from dirichlet
  end
  % - once again, but it's transposed this time, so backward sub:
  yAprev = zeros(Np,1);
  for t=NT:-1:1
    yA = Apinv\y1((1:Np)+(t-1)*Np);
    yM = Mpinv\y1((1:Np)+(t-1)*Np);
    yAWM = Apinv\((W{t})'*yM);
%     yM = Mpinv\y1((1:Np)+(t-1)*Np);
    y1((1:Np)+(t-1)*Np) = yA/dt+mu*yM + yAWM - yAprev/dt;  % yAprev is zero at first it
    yAprev = yA;
    yAprev(dirIdx) = 0; % kill contributions from dirichlet
  end
  
  % Once again:
  % apply B'
  y2 = zeros(Nu*NT,1);
  for t=1:NT
    y2((1:Nu)+(t-1)*Nu) = B'*y1((1:Np)+(t-1)*Np);
  end
  % solve for Fu via time stepping - but it's transposed this time, so backward sub:
  yprev = zeros(Nu,1);
  for t=NT:-1:1
    if length(Fuinv) == 1
      yprev = Fuinv{1}\( y2((1:Nu)+(t-1)*Nu) + Mu*(yprev/dt) );
    else
      yprev = (Fuinv{t})'\( y2((1:Nu)+(t-1)*Nu) + Mu*(yprev/dt) );
    end
    y2((1:Nu)+(t-1)*Nu) = yprev;
  end
  % apply B
  y = zeros(Np*NT,1);
  for t=1:NT
    y((1:Np)+(t-1)*Np) = B*y2((1:Nu)+(t-1)*Nu);
  end

end






refLvls = -2:-1:-5; % must be an array of descending, consecutive numbers
sigma = cell(1,length(refLvls));
colors = {'blue', 'red', 'green', 'black'}; % 4 should suffice?

path0 = strcat('Pb',int2str(Pb),'_Prec',int2str(Prec),'_STsolve',int2str(STsolve),...
             '_oU',int2str(oU),'_oP',int2str(oP));
if Pb==4
  path0 = strcat(path0, '_Pe', num2str(Pe,'%8.6f'));
end

for i = 1:length(refLvls)
  dt = 2^refLvls(i);
  path = strcat(path0,'/dt',num2str(dt,'%8.6f'),'_r',int2str(r),'_');

  filename = strcat(path, 'B.dat');
  B  = spconvert(load(filename)) / dt;    % B is assembled as if multiplied by dt, so rescale it
  filename = strcat(path, 'Ap.dat');
  Ap = spconvert(load(filename));
  filename = strcat(path, 'Mp.dat');
  Mp = spconvert(load(filename));
  filename = strcat(path, 'Mu.dat');
  Mu = spconvert(load(filename));

  % TODO: should this go *after* I set one value on pressure to avoid singular operator?
  % find dirichlet nodes on pressure
  dirIdx = [];
  for kk=1:size(Ap,1)
    if (nnz(Ap(:,kk)) == 1)
      dirIdx =[dirIdx,kk];
    end
  end
  
  if Pb == 1 || Pb == 4          % Ap is singular in that case: tweak last row/col to make it invertible
    Ap(:,end) = sparse(size(Ap,1),1);
    Ap(end,:) = sparse(1,size(Ap,2));
    Ap(end,end) = 1;
  end
  
  Apinv = decomposition(Ap, 'chol');
  Mpinv = decomposition(Mp, 'chol');

  N = size(Mp,1);
  Nk = 2^(-refLvls(i));
  
  sigma{i} = zeros(N*Nk,1);

  if Pb==4
    Fuinv = cell(Nk,1);
    W    = cell(Nk,1);
    for k = 1:Nk
      filename = strcat(path, 'Fu_', int2str(k-1), '.dat');
      Fu = spconvert(load(filename)) / dt;    % Fu is assembled as if multiplied by dt, so rescale it
      Fuinv{k} = decomposition(Fu, 'lu');
      filename = strcat(path, 'Wp_', int2str(k-1), '.dat');
      W{k} = spconvert(load(filename));    % don't rescale!
    end   
  else
    Fuinv = cell(1,1);
    W    = cell(Nk,1);
    filename = strcat(path, 'Fu_0.dat');
    Fu = spconvert(load(filename)) / dt;    % Fu is assembled as if multiplied by dt, so rescale it
    Fuinv{1} = decomposition(Fu, 'lu');
    for k = 1:Nk
      W{k} = sparse(N,N);    % W is not used: leave it as zero
    end
  end


  currentPrecOp = @(x) precOp( x, Mpinv, Apinv, B, dt, Fuinv, Mu, W, dirIdx );

  sigma{i} = sqrt( eigs( currentPrecOp, Nk*N, Nk*N ) );
%   sigma{i} = sqrt( eigs( currentPrecOp, Nk*N, ceil(N/4), 'bothendsreal' ) );
  
  scatter(sigma{i}, zeros(size(sigma{i})), colors{i})
  set(gca,'xscale','log')
  pause(0.01)
  hold on
end

out  = ones(length(sigma{end}),length(refLvls))*NaN;
nsubsmpl = min(N,100);
out2 = zeros(nsubsmpl,2*length(refLvls));
for i=1:length(refLvls)
  out(1:length(sigma{i}), i) = sigma{i};
  % perform  subsampling as well:
  out2(:,i) = sigma{i}( randperm(length(sigma{i}), nsubsmpl) );
end

filename = strcat(path0,'/singVals_r',int2str(r),'.dat');
format = [ repmat(' %20.18f', [1,size(out,2)] ), '\n' ];
fileID = fopen(filename,'w');
fprintf(fileID,format,out');
fclose(fileID);

filename = strcat(path0,'/singValsSub_r',int2str(r),'.dat');
format = [ repmat(' %20.18f', [1,size(out2,2)] ), '\n' ];
fileID = fopen(filename,'w');
fprintf(fileID,format,out2');
fclose(fileID);


end