## Space-time Block Preconditioning for incompressible flow

This repository contains the source code used to run the experiments reported in
the manuscript
> *Space-time Block Preconditioning for incompressible flow*\
> by [F. Danieli](https://www.maths.ox.ac.uk/people/federico.danieli),
> [B.S. Southworth](http://ben-southworth.science/),
> and [A.J. Wathen](https://www.maths.ox.ac.uk/people/andy.wathen).



### Installation requirements
For the code to run properly, it requires [PETSc](https://www.mcs.anl.gov/petsc/ "PETSc"),
[hypre](https://computing.llnl.gov/projects/hypre-scalable-linear-solvers-multigrid-methods/software "hypre")
and [MFEM](https://mfem.org/ "MFEM") to be installed and linked accordingly.
Cross-dependencies might pose some challenges: we've found that installing PETSc
first, setting it up so that it automatically includes all common dependencies
(metis, hypre, and mumps), and then installing MFEM, helps significantly in this
regard. To install PETSc with the required packages, first download the source files,
then configure and install it via
```
cd <petsc_directory>
./configure --download-hypre --download-metis --download-mumps
make PETSC_DIR=<petsc_directory> PETSC_ARCH=<petsc_arch_name>
```
Next comes MFEM. After downloading the source files, configure and install it via
```
cd <MFEM directory>
make BUILD_DIR=<MFEM-build-directory> config MFEM_USE_MPI=YES MFEM_USE_PETSC=YES MFEM_USE_METIS_5=YES PETSC_ARCH=<petsc_arch_name> PETSC_DIR=<petsc_directory>/<petsc_arch_name> HYPRE_DIR=<petsc_directory>/<petsc_arch_name>/externalpackages/git.hypre/src/hypre METIS_DIR=<petsc_directory>/<petsc_arch_name>/externalpackages/git.metis/petsc-build
cd <MFEM-build-directory>
make
```
With this, all the required software should be installed. For a quick (or a more in-depth)
check on MFEM installation, run
```
cd <MFEM-build-directory>
make check
(make test)
```


### Description of source files
- `test.cpp`: Main file. Responsible for set-up of experiments, system assembly, (nonlinear) solver invoke.
- `stokesstoperatorassembler.cpp/hpp`: Most relevant class. Assembly of space-time FE operators and space-time pressure Schur complement.
- `spacetimesolver.cpp/hpp`: Solver for space-time velocity block via time-stepping. Encapsulated so that it allows to easily swap this with other solvers (OOP principles)
- `vectorconvectionintegrator.cpp/hpp`: Utility class for assembly of FE convection operator for Oseen.

###### Other relevant files
- `rc_SpaceTimeStokes_*`: Option files for various PETSc solvers configurations
- `experiments.sh`: Convenience script file for running a sequence of simulations for various refinement levels
- `/results/rescon.m`: Matlab plot of GMRES residual convergence for various refinement levels - provided the corresponding `convergence_results_*` folder has been generated
- `/results/Operators/computeEigs.m`: Matlab computation and plot of eigs of space-time operatos - provided the corresponding matrices have been generated and saved
- `/ParaView/velocityPlots.pvsm` and `/ParaView/pressurePlots.pvsm`: ParaView state files for plotting FE soltions.
- `/Meshes/*`: Files containing mesh information for discretised domains and problems BC.

### Running experiments
After compilation, running `./test -h` via terminal prints a thorough description
of all the options accepted by the executable. In this section, we report the
commands used for generating the various result tables/plots appearing in the
manuscript


- ##### Figure 1
Eigenvalue distribution of the relevant operators appearing in the space-time
preconditioned system, with the setup for problem 4, and varying values of ∆t,
∆x, and Pe. As a first step, we need to generate and save the relevant FE
matrices. This is done by running
```
mpirun -np <2^-i> ./test -r <j> -V -1 -Pb 1 -petscopts rc_SpaceTimeStokes_SingAp
mpirun -np <2^-i> ./test -r <j> -V -1 -Pb 4 -Pe <k> -petscopts rc_SpaceTimeStokes_SingAp
```
with `i=3,5` (tweaking time refinemenet), `j=4,6` (tweaking space refinement),
and `Pe=k=10,100` (tweaking Peclet number). Notice one can use the shell file
`experiments.sh` to run a sequence of tests with varying levels of refinement:
the shell provides a skeleton for doing so, but it can be modified accordingly
quite easily (remember to run `chmod +x experiments.sh` to make it executable).
The option `-Pb` identifies the problem: `-Pb 1` is driven cavity flow, `-Pb 2`
is Poiseuille, `-Pb 3` is flow over backward-facing step, `-Pb 4` is double-glazing
flow. Notice choosing `-Pb 1`corresponds to `-Pb 4, -Pe 0`, but the latter is not
a valid command, since for `-Pb 4` the program automatically imposes a `-Pe > 0`.
The option `-V -1` is responsible for flagging eigenvalue analysis: this will
both print a list of (approximate) eigenvalues for the preconditioned
system, computed automatically by PETSc in the solution procedure, and (most
relevantly) will save in `/results/Operators/` all the relevant FE matrices for the
requested parameters. Notice this takes up quite some memory, especially for
`Pb=4`, since an operator is printed *per each time step*. To get a better
eigenvalue approximation, one can then use the MATLAB script `/results/Operators/computeEigs.m`, which applies some additional simplifications before computing
them numerically.



- ##### Figure 2 to 5
Example solutions for the various problems considered. The figures are generated
using ParaView, starting from the results of the simulations
```
mpirun -np 16 ./test -r 5 -Pb 1 -out 3 -petscopts rc_SpaceTimeStokes_SingAp
mpirun -np 16 ./test -r 5 -Pb 2 -out 3 -petscopts rc_SpaceTimeStokes
mpirun -np 16 ./test -r 5 -Pb 3 -out 3 -petscopts rc_SpaceTimeStokes
mpirun -np 16 ./test -r 5 -Pb 4 -out 3 -Pe 10 -petscopts rc_SpaceTimeStokes_SingAp
```
Notice that `Pb 1` and `Pb 4` have Dirichlet BC on velocity everywhere: this
gives rise to a singular pressure stiffness matrix Ap, hence the extra `_SingAP`
in the options for PETSc. Option `-out 3` is responsible for exporting the
solution for ParaView: be mindful about memory usage for finer meshes. These can
be found in `/ParaView/STstokes_*/`. For Poiseuille `-Pb 2`, this also generates
the analytical solution in `/ParaView/STstokes_Poiseuille_Ex/`. Once the
solutions have been saved, one can load the ParaView states
`/ParaView/pressurePlots.pvsm` and `/ParaView/velocityPlots.pvsm` to include
streamlines and contourplots.



- ##### Table 1
Number of GMRES iterations to convergence for different refinement levels,
problems, and solvers employed. To fill the columns on the left (ideal solvers),
one uses
```
mpirun -np <2^-i> ./test -r <j> -Pb 1 -out 1 -petscopts rc_SpaceTimeStokes_SingAp
mpirun -np <2^-i> ./test -r <j> -Pb 2 -out 1 -petscopts rc_SpaceTimeStokes
mpirun -np <2^-i> ./test -r <j> -Pb 3 -out 1 -petscopts rc_SpaceTimeStokes
mpirun -np <2^-i> ./test -r <j> -Pb 4 -out 1 -Pe 10 -petscopts rc_SpaceTimeStokes_SingAp
```
while to fill the columns on the right (approximate solvers), one uses
```
mpirun -np <2^-i> ./test -r <j> -ST 2 -Pb 1 -out 1 -petscopts rc_SpaceTimeStokes_FGMRES_approx2_SingAp
mpirun -np <2^-i> ./test -r <j> -ST 2 -Pb 2 -out 1 -petscopts rc_SpaceTimeStokes_FGMRES_approx2
mpirun -np <2^-i> ./test -r <j> -ST 2 -Pb 3 -out 1 -petscopts rc_SpaceTimeStokes_FGMRES_approx2
mpirun -np <2^-i> ./test -r <j> -ST 2 -Pb 4 -out 1 -Pe 10 -petscopts rc_SpaceTimeStokes_FGMRES_approx2_SingAp
```
with `i=1,...,7`, `j=2,...,8` in both cases. Option `-ST 2` flags the use of
GMRES + BoomerAMG (AIR) for the solution of the space-time velocity block,
rather than sequential time-stepping (`-ST 0`, default). Notice since we're
using GMRES inside a Krylov iteration, we need to flag the use of FGMRES as an
outer solve (the `-petscopts` option has changed accordingly). Also notice that
`Pb 1` and `Pb 4` have Dirichlet BC on velocity everywhere: this gives rise to a
singular pressure stiffness matrix Ap, hence the extra `_SingAP` in the options
for PETSc. The flag `_approx2` denotes use of approximate solvers also for the
internal blocks of the preconditioner (pressure mass and stiffness matrix).\
Running these simulations with the option `-out 1` generates some
`/results/convergence_results_*.txt` files with number of iterations to
convergence for each problem setup. A more
detailed file containing residual evolution can be found in the folder
`/results/convergence_results_*/`.


- ##### Figure 6
Details on GMRES residual evolution for problem 1. Similarly to above, one can
run
```
mpirun -np <2^-i> ./test -r <j> -Pb 1 -out 2 -petscopts rc_SpaceTimeStokes_SingAp
```
The option `-out 2` additionally stores in the folders
`/results/convergence_results_*/` some extra files with the norm of the residual
as it evolves during the iterations. A plot in MATLAB can then be generated using
`/results/rescon.m`.



- ##### Table 2
Number of iterations to convergence for varying Pe. Once again, we are running
```
mpirun -np <2^-i> ./test -r <j> -Pb 1 -out 1 -Pe <2^k> -petscopts rc_SpaceTimeStokes_SingAp
```
for `i=4,...,7`, `j=1,...,8`. The value of the Peclet number is adjusted by the
parameter `Pe`: for these simulations, `k=4,...,8`.



- ##### Table 3
Number of (outer) Picard and (inner average) GMRES iterations to convergence for
Navier-Stokes. This time we run
```
mpirun -np <2^-i> ./test -r <j> -Pb 11 -out 2 -petscopts rc_SpaceTimeStokes_SingAp
mpirun -np <2^-i> ./test -r <j> -Pb 13 -out 2 -petscopts rc_SpaceTimeStokes
```
Adding the extra `1` digit in the `-Pb` option identifies the corresponding N-S
version of the problem. For ease of implementation, the code in this case is
much less flexible, and tolerance for Picard is hard-coded. Convergence evolution
information is saved in the folders `/results/Picard_convergence_results_*/`
(for details on residual evolution) or in the files
`/results/Picard_convergence_results_*.txt` (for overview info on number of
iterations for inner and outer solve).


- ##### Table 4
Here compute a ratio between the number of iterations necessary to achieve convergence
using our block space-time preconditioner VS sequential time-stepping. The first
number is already available from Table 1; while we can recover the second by running
```
mpirun -np <2^-i> ./test -r <j> -ST 9 -Pb 1 -out 1 -petscopts rc_SpaceTimeStokes_SingAp_noTol
mpirun -np <2^-i> ./test -r <j> -ST 9 -Pb 2 -out 1 -petscopts rc_SpaceTimeStokes_noTol
mpirun -np <2^-i> ./test -r <j> -ST 9 -Pb 3 -out 1 -petscopts rc_SpaceTimeStokes_noTol
mpirun -np <2^-i> ./test -r <j> -ST 9 -Pb 4 -out 1 -Pe 10 -petscopts rc_SpaceTimeStokes_SingAp_noTol
```
The option `-ST 9` bypasses many of the others, and flags the use of sequential
time-stepping for the *whole* system. Many of the options for the solvers are
similar to the all-at-once case, but we need to impose a stricter tolerance of
convergence for each time-step, if we aim to recover a more fair comparison.
This is why we add the option `_noTol` to the PETSc option file: this way the
tolerance for the solver is computed internally (depending on number of time-steps)
rather than being prescribed outside.
